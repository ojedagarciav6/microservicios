package com.microservicios.bikeservice.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservicios.bikeservice.entity.Bike;
import com.microservicios.bikeservice.service.BikeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/bike")
@Slf4j
public class BikeController {
	
	@Autowired
	BikeService bikeService;
	
	@GetMapping
	public ResponseEntity<List<Bike>> getAll(){
		
		List<Bike> bikes = bikeService.getAll();
		
		if(bikes.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(bikes);
		
	}
	
	@GetMapping("/{bikeId}")
	public ResponseEntity<Bike> getBikeById(@PathVariable("bikeId") int id){
		Bike bike = bikeService.getBikeById(id);
		
		if(bike == null)
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(bike);
	}
	
	@PostMapping()
	public ResponseEntity<Bike> save(@RequestBody Bike bike){
		
		Bike newBike = bikeService.save(bike);
		
		return ResponseEntity.ok(newBike);
	}
	
	@GetMapping("/byuser/{userId}")
	public ResponseEntity<List<Bike>> getCarsByUserId(@PathVariable("userId") int id){
		log.info("Entramos en controlador");
		List<Bike> bikeList = bikeService.getBikesByUserId(id); 
		
		if(bikeList.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(bikeList) ;
	}

}
