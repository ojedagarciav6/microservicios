package com.microservicios.bikeservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservicios.bikeservice.entity.Bike;
import com.microservicios.bikeservice.repository.BikeRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BikeService {

	@Autowired
	BikeRepository bikeRepository;

	public List<Bike> getAll() {
		return bikeRepository.findAll();
	}

	public Bike getBikeById(int id) {

		return bikeRepository.findById(id).orElse(null);

	}

	public Bike save(Bike bike) {

		Bike newBike = bikeRepository.save(bike);
		return newBike;
	}
	
	public List<Bike> getBikesByUserId(int userId){
		log.info("Entramos en el servicio");
		return bikeRepository.getByUserId(userId);
	}
}
