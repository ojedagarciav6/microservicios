package com.microservicios.carservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservicios.carservice.entity.Car;
import com.microservicios.carservice.service.CarService;

@RestController
@RequestMapping("/car")
public class CarController {
	
	@Autowired
	CarService carService;

	@GetMapping
	public ResponseEntity<List<Car>> getAll(){
		
		List<Car> carList = carService.getAll(); 
		
		if(carList.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(carList) ;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Car> getCarById(@PathVariable("id") int id){
		
		Car car = carService.getCArById(id);
		
		if(car == null)
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(car);
		
	}
	
	@PostMapping()
	public ResponseEntity<Car> save(@RequestBody Car car){
		Car newCar = carService.save(car);
		
		return ResponseEntity.ok(newCar);
	}
	
	@GetMapping("/byuser/{userId}")
	public ResponseEntity<List<Car>> getCarsByUserId(@PathVariable("userId") int userId){
		
		List<Car> carList = carService.findByUserId(userId); 
		
		if(carList.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(carList) ;
	}
	
}
