package com.microservicios.carservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservicios.carservice.entity.Car;
import com.microservicios.carservice.repository.CarRepository;

@Service
public class CarService {

	@Autowired
	CarRepository carRepository;

	public List<Car> getAll() {
		return carRepository.findAll();
	}

	public Car getCArById(int id) {
		return carRepository.findById(id).orElse(null);
	}

	public Car save(Car car) {
		Car newCar = carRepository.save(car);

		return newCar;
	}
	
	public List<Car> findByUserId(int userId){
		return carRepository.findByUserId(userId);
	}

}
