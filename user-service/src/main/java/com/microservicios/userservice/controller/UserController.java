package com.microservicios.userservice.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservicios.userservice.entity.User;
import com.microservicios.userservice.model.Bike;
import com.microservicios.userservice.model.Car;
import com.microservicios.userservice.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping
	public ResponseEntity<List<User>> getAll() {
		List<User> users = userService.getAll();

		if (users.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(users);
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getById(@PathVariable("id") int id) {
		User userById = userService.findUserById(id);

		if (userById == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(userById);
	}

	@PostMapping()
	public ResponseEntity<User> save(@RequestBody User user) {
		User userNew = userService.save(user);

		return ResponseEntity.ok(userNew);
	}

	@GetMapping("/cars/{userId}")
	public ResponseEntity<List<Car>> getCars(@PathVariable("userId") int userId) {

		User user = userService.findUserById(userId);

		if (user == null)
			return ResponseEntity.notFound().build();
		List<Car> cars = userService.getCarsByUserId(userId);
		return ResponseEntity.ok(cars);

	}

	@GetMapping("/bikes/{userId}")
	public ResponseEntity<List<Bike>> getBikes(@PathVariable("userId") int userId) {
		User user = userService.findUserById(userId);

		if (user == null)
			return ResponseEntity.notFound().build();
		List<Bike> bikes = userService.getBikesByUserId(userId);
		return ResponseEntity.ok(bikes);
	}
	
	@PostMapping("/savecar/{userId}")
	public ResponseEntity<Car> saveCar(@PathVariable("userId") int userId, @RequestBody Car car){
		Car carNew = userService.save(userId, car);
		
		return ResponseEntity.ok(carNew);
	}
	
	@PostMapping("/savebike/{userId}")
	public ResponseEntity<Bike> saveBike(@PathVariable("userId") int userId, @RequestBody Bike bike){
		Bike bikeNew = userService.save(userId, bike);
		
		return ResponseEntity.ok(bikeNew);
	}
	
	@GetMapping("/getall/{userId}")
	public ResponseEntity<Map<String,Object>> getAllVehicles(@PathVariable("userId") int userId){
		
		Map<String, Object> result = userService.getUserAndVehicles(userId);
		
		return ResponseEntity.ok(result);
		
	}
}
