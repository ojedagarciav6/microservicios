package com.microservicios.userservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.microservicios.userservice.entity.User;
import com.microservicios.userservice.feignclients.BikeFeignClient;
import com.microservicios.userservice.feignclients.CarFeignClient;
import com.microservicios.userservice.model.Bike;
import com.microservicios.userservice.model.Car;
import com.microservicios.userservice.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	CarFeignClient carFeignClient;
	
	@Autowired
	BikeFeignClient bikeFeignClient;

	public List<User> getAll() {
		return userRepository.findAll();
	}

	public User findUserById(int id) {

		return userRepository.findById(id).orElse(null);

	}

	public User save(User user) {
		User userNew = userRepository.save(user);

		return userNew;
	}

	public List<Car> getCarsByUserId(int userId) {

		List<Car> cars = restTemplate.getForObject("http://localhost:8002/car/byuser/" + userId, List.class);
		return cars;
	}

	public List<Bike> getBikesByUserId(int userId) {

		List<Bike> bikes = restTemplate.getForObject("http://localhost:8003/bike/byuser/" + userId, List.class);
		return bikes;
	}
	
	public Car save(int userId, Car car) {
		car.setUserId(userId);
		Car carNew = carFeignClient.save(car);
		
		return carNew;
	}
	
	public Bike save(int userId, Bike bike) {
		bike.setUserId(userId);
		Bike bikeNew = bikeFeignClient.save(bike);
		
		return bikeNew;
	}
	
	public Map<String, Object> getUserAndVehicles(int userId){
		
		Map<String, Object> result = new HashMap<>();
		
		User user = userRepository.findById(userId).orElse(null);
		
		if(user == null ) {
			result.put("mensaje", "No se ha encontrado el usuario");
			
			return result;
		}
		
		result.put("User", user);
		
		List<Car> cars = carFeignClient.getCars(userId);
		
		if(cars.isEmpty())
			result.put("Cars", "Este user no tiene cars");
		else
			result.put("Cars", cars);
		
		List<Bike> bikes = bikeFeignClient.getBikes(userId);
		
		if(bikes.isEmpty())
			result.put("Bikes", "Este user no tiene bikes");
		else
			result.put("Cars", bikes);
		
		return result;
		
	}
	
	

}
