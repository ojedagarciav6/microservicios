package com.microservicios.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.microservicios.userservice.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
